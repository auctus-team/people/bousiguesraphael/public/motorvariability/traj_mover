# Links
- Sonarqube : https://sonarqube.inria.fr/sonarqube/dashboard?id=auctus%3Apanda%3Apanda-traj
- Documentation : https://auctus.gitlabpages.inria.fr/panda/panda_traj/index.html


# Traj Mover

Library using KDL to generate a path in space
Used to give pose command to the controller at each time during a MOVER Experiment. Traj Mover dependency allows user to use kdl_trajectory independently without worrying about traj_mover.
The library is ROS independent (except for the ROS STREAM and ERROR types).

To learn more about this project, read the [wiki TrajMover](https://gitlab.inria.fr/auctus-team/people/bousiguesraphael/public/motorvariability/traj_mover/-/wikis/home) and the wiki [TorqueQP](https://gitlab.inria.fr/auctus-team/people/bousiguesraphael/public/motorvariability/torque_qp/-/wikis/home).

# Usage

## Path Functions

### PathFunction()
`KDL::Vector TrajMover::PathFunction(traj_mover::MoverProperties& mover_properties_ ,double s)`

 Returns a vector $`X(s)`$ corresponding to the position of the center of control frame for the specified reference abscissa $`s`$. The choice of the path description is made thanks to `mover_properties/path_no`.

### PathDerivative()
`KDL::Vector TrajMover::PathDerivative(traj_mover::MoverProperties& mover_properties_ ,double s)`

Returns a vector $`\dot X(s)`$ corresponding to the derivative of control frame center position for the specified reference abscissa $`s`$. The choice of the path description is made thanks to `mover_properties/path_no`.


### PathDoubleDerivative()
`KDL::Vector TrajMover::PathDoubleDerivative(traj_mover::MoverProperties& mover_properties_ ,double s)`

Returns a vector $`\ddot X(s)`$ corresponding to the second derivative of control frame center position for the specified reference abscissa $`s`$. The choice of the path description is made thanks to `mover_properties/path_no`.


## Other Functions
### Init()
`void TrajMover::Init(traj_mover::MoverProperties& mover_properties_)`

Initializes the `follower` and `follow_init` flags to False and specifies the position on the robot in real world frame in a local variable.

### BuildInitFollow()
`void TrajMover::BuildInitFollow(kdl_trajectories::TrajProperties &traj_properties_, traj_mover::MoverProperties& mover_properties_, pinocchio::SE3 X_curr)`

Initializes a trajectory to reach the first pose on the path from current pose.

### ReachInitFrame()
`void TrajMover::ReachInitFrame(kdl_trajectories::TrajProperties& traj_properties_, traj_mover::MoverProperties& mover_properties_, pinocchio::SE3 oMtip)`

Checks the distance between current pose and first pose on the path. If the error is sufficiently low, `follow_init` flag is set to True.

### updatePose()
`void TrajMover::updatePose(kdl_trajectories::TrajProperties& traj_properties_, traj_mover::MoverProperties& mover_properties_, double time_dt)`

If the controller is asking for MOVER features, this function returns $`X_{traj}`$, $`\dot X_{traj}`$ and $`\ddot X_{traj}`$ to it depending on the value of `follow_init` flag.

### Distance()
`double TrajMover::Distance(traj_mover::MoverProperties& mover_properties_ ,KDL::Frame &X_curr_, double &s)`

Computes the distance between the current frame and a frame on the path, specified by `s`. This distance is either computed in term of linear euclidean distance, either in term of angular distance, both pondered by a coefficient $`\alpha`$.

### FollowSampledPath()
`double TrajMover::FollowSampledPath(kdl_trajectories::TrajProperties &traj_properties_,traj_mover::MoverProperties& mover_properties_, KDL::Frame &X_curr_, KDL::Twist &Xd_curr_)`

Determines the next reference abscissa to reach, with respect to robot maximal velocity (hardcoded in the function) and depending on the direction of end-effector movement, by prospecting from near to near around the current abscissa.

### PathTangentVector()
`KDL::Vector TrajMover::PathTangentVector(traj_mover::MoverProperties& mover_properties_ ,double s)`

Computes the normalized tangent vector on the path at the specified abscissa $`s`$ using `PathDerivative()` function.

### PathNormalVector()
`KDL::Vector TrajMover::PathNormalVector(traj_mover::MoverProperties& mover_properties_ ,double s)`

Computes a normalized normal vector on the path at the specified abscissa $`s`$ using `PathDoubleDerivative()` function.


### PathFrame()
`KDL::Frame TrajMover::PathFrame(traj_mover::MoverProperties& mover_properties_ ,double s)`

Compute control frame on the path at the specified abscissa $`s`$ using `PathTangentVector()` as $`y_c`$ axis. 

$`z_c`$ axis is defined like:

```math
z_c \cdot y_c = 0\\
z_c \cdot x_0 = 0\\
z_c \cdot z_0 \leq 0 \\
\|z_c\| = 1
```

(with $`(x_0\;y_0\;z_0`$ the root/robot base frame axis)

$`x_c`$ axis is defined like:

```math
y_c \times z_c = x_c
```



### PathCurvature()
`double TrajMover::PathCurvature(traj_mover::MoverProperties& mover_properties_ ,double &s)`

Computes the path curvature $`c`$ at the given reference abscissa $`s`$.

```math
c = \sqrt\frac{ \| \dot X(s) \| ^ 2 * \| \ddot X(s) \|^2 - (\dot X(s) \cdot \ddot X(s))^2)}{ \| \dot X(s) \|^ 6}
```

### publishTrajectoryMover()
`kdl_trajectories::PublishTraj TrajMover::publishTrajectoryMover(traj_mover::MoverProperties& mover_properties_)`

Publishes the computed path to plot in Rviz. Called in controllers function `publishTrajectory()`.

## Unused/Obsolete functions

### PathPreSamplerInit()
`void TrajMover::PathPreSamplerInit(kdl_trajectories::TrajProperties& traj_properties_, traj_mover::MoverProperties& mover_properties_,double k, double dt)`

Build a list of reference abscissa to sample the path according to robot specifications (velocity) by calling `PathSampler()` function.

### PathSampler()
`double TrajMover::PathSampler(kdl_trajectories::TrajProperties &traj_properties_,traj_mover::MoverProperties& mover_properties_ , double s_curr, double k, double dt)`

Computes and returns successive reference abscissa knowing robot specifications (velocity in translation and in orientation for its end-effector, hardcoded in the function). This function consider also use of Power Law (see Francesco Lacquaniti, Carlo Terzuolo, et Paolo Viviani, « The Law Relating the Kinematic and Figural Aspects of Drawing Movements », Acta Psychologica 54, nᵒ 1‑3 (octobre 1983): 115‑30, https://doi.org/10.1016/0001-6918(83)90027-6.) by adjusting parameter $`k`$.


## MoverProperties structure
MoverProperties has the following structure:
```
    bool follower_
    bool follower_demo_
    geometry_msgs/Pose X_curr_
    geometry_msgs/Pose X_des_jog_
    geometry_msgs/Twist Xd_curr_
    geometry_msgs/Twist X_var
    geometry_msgs/Wrench W_ext
    float64 alpha
    float64 beta
    float64 delta
    float64 curv
    float64 tolerance_
    bool follow_init_
    bool return_to_start_
    float64 delta_s_
    float64 k_PLconstant_
    int64 controller_mode_
    float64 s_next_
    float64 nb_samples_
    geometry_msgs/Pose oMroot_
    int64 r_sampling
    int64 path_no
```

If `follower` == **True**, and `follow_init` == **False**, `ReachInitFrame()` will publish trajectory to the first pose on the path ($`s=0`$). If this pose is reached, the function will switch `follow_init` to **True**.

If `follower_demo` == **True**, `follower` is switched to **True**. If `follow_init` == **False**, `ReachInitFrame()` will publish trajectory to the first pose on the path ($`s=0`$). If this pose is reached, the function will switch `follow_init` to **True**.

If `return_to_start` == **True** and `follower` == **True**, the controller will ask traj_mover for control pose until reaching the first pose on the path.

`alpha` is the angle for which the robot and-effector will move between two poses.

`delta` is the linear distance for which the robot end-effector will move between two poses.

`curv` uses `PathCurvature()` function to return current path curvature on the considered reference abscissa

`nb_samples` specifies the number of frames describing the path from $`s=0`$ to $`s=1`$ 

`oMroot_` describes the transformation from world real frame, to robot base frame, so an offset to add to `PathFunction()`

`r_sampling` specifies the number of frames describing the path from $`s=0`$ to $`s=1`$ in the `torque_qp/config/torque_qp.yaml`

`path_no` specifies the number of current path to considered
