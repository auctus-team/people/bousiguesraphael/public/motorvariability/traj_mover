
//#ifndef TRAJMOVER_HPP
#define TRAJMOVER_HPP


// TrajectoryGenerator
#include <kdl_trajectories/kdl_trajectories.hpp>
#include <kdl_trajectories/TrajProperties.h>

// Mover Path
#include <traj_mover/MoverProperties.h>


// Eigen
#include <Eigen/Dense>

// Transformations
#include <eigen_conversions/eigen_msg.h>
#include <eigen_conversions/eigen_kdl.h>

// Pinocchio 
#include "pinocchio/fwd.hpp"
#include "pinocchio/parsers/urdf.hpp"
#include "pinocchio/algorithm/jacobian.hpp"
#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
#include "pinocchio/algorithm/frames.hpp"
#include "pinocchio/multibody/model.hpp"


class TrajMover : public TrajectoryGenerator
{
    public:

    
    void updatePose(kdl_trajectories::TrajProperties& traj_properties_, traj_mover::MoverProperties& mover_properties_, double time_dt);

    /**
    * @brief Prepare curvilinear abscissa tab for follower mode with constant sampling of offline sampling
    */
    void PathPreSamplerInit(kdl_trajectories::TrajProperties& traj_properties_, traj_mover::MoverProperties& mover_properties_,double k, double dt);

    //----------------------------------------------------
    // INIT FUNCTIONS
    //----------------------------------------------------
    void Init(traj_mover::MoverProperties& mover_properties_);

    void ReachInitFrame(kdl_trajectories::TrajProperties& traj_properties_, traj_mover::MoverProperties& mover_properties_, pinocchio::SE3 X_curr);

    void BuildInitFollow(kdl_trajectories::TrajProperties &traj_properties_, traj_mover::MoverProperties& mover_properties_, pinocchio::SE3 X_curr);


    // ---------------------------------------------------
    // Path Functions
    // ---------------------------------------------------

    /**
     * \brief
     * Define a path with a function, Compute frame corresponding to a curvilinear abscissa on the path
     */
    KDL::Vector PathFunction(traj_mover::MoverProperties& mover_properties_ , double s);
    KDL::Vector PathDerivative(traj_mover::MoverProperties& mover_properties_ ,double s);
    KDL::Vector PathDoubleDerivative(traj_mover::MoverProperties& mover_properties_ ,double s);

    // ---------------------------------------------------
    // Frames Build Functions
    // ---------------------------------------------------
    KDL::Vector PathTangentVector(traj_mover::MoverProperties& mover_properties_ ,double s);
    KDL::Vector PathNormalVector(traj_mover::MoverProperties& mover_properties_ ,double s);
    KDL::Frame PathFrame(traj_mover::MoverProperties& mover_properties_ ,double s);

    // ---------------------------------------------------
    // Publish Traj Function
    // ---------------------------------------------------
    kdl_trajectories::PublishTraj publishTrajectoryMover(traj_mover::MoverProperties& mover_properties_);

    // ---------------------------------------------------
    // Dist Frame Function
    // ---------------------------------------------------
    double Distance(traj_mover::MoverProperties& mover_properties_ ,KDL::Frame &X_curr_, double &s);
    
    
    
    /**
    * \fn bool FollowSampledPath
    * \brief Load a rosparam to a variable
    * \param const std::string& param_name the name of the ros param
    * \param Eigen::VectorXd& param_data a Eigen vector linked to the std::vector ros param
    * \return true if the ros param exist and can be linked to the variable
    */
    double FollowSampledPath(kdl_trajectories::TrajProperties &traj_properties_, traj_mover::MoverProperties& mover_properties_,KDL::Frame &X_curr_, KDL::Twist &Xd_curr_);
    double PathCurvature(traj_mover::MoverProperties& mover_properties_ ,double &s);
    double PathSampler(kdl_trajectories::TrajProperties &traj_properties_, traj_mover::MoverProperties& mover_properties_ ,double s_curr, double k, double dt);

    ////////////////////////////////////////////////////////////////////////////////////////////////
    //------------------------------------PRIVATE PART----------------------------------------------
    ////////////////////////////////////////////////////////////////////////////////////////////////

    private:
    /*KDL::Trajectory_Composite *ctraject;
    KDL::Frame task_frame, X_traj_, X_curr_, X_des_jog_;
    KDL::Twist Xd_traj_, Xdd_traj_;*/

    // ROBOT STATE
    KDL::Twist Xd_curr_;


    bool follower_=false;
    bool follow_init_=false;
    double reach_index = 0;
    bool presampler_initialized_ = false;

    std::vector<double> curvilinear_abscissa_tab_; // contains index of every path frames

    Eigen::Affine3d oMroot;  // transformation from world frame to robot base frame (root link)
    KDL::Frame oMroot_kdl; // Root frame in world frame in KDL 
    KDL::Vector oProot_kdl; // Position of origin of root frame in world frame in KDL

    // Trajectory variables
    TrajectoryGenerator trajectory_panda; /*!< @brief TrajectoryMover object */
    double s_prev = 0;

};