#!/usr/bin/env python
# coding: utf-8

# In[1]:


import matplotlib.pyplot as plt
import numpy as np
import csv
import scipy.integrate as integ
import sympy as sp
import sympy.plotting as splot
import pandas as pd

from scipy import misc

from numpy import cos,sin,pi, arccos
from numpy.linalg import inv

from mpl_toolkits import mplot3d 

from sympy.sets import Interval

from sklearn import datasets

from sympy import solveset, symbols, Interval, Min



ft=24 ; #Police size


# In[13]:


n_path=13; # path number in "TrajectoryCharacteristics.ods" file
# VARIABLES
nb_points = 150 ;

#A*s+B*sp.sin(f1*s)+C*sp.cos(f1*s)+D*sp.sin(f2*s)**2+E*sp.cos(f2*s)**2+F*sp.sin(f3*s)**3+G*sp.cos(f3*s)**3 + H*s**2 + I*s**3
A=-2;         #        #0
B=0;         # sin    #0
C=0;         # cos    #3/2
D=0;         # sin^2  #0
E=0;         # cos^2  #2
F=0;         # sin^3  #0
G=0;         # cos^3  #0
H=0;         # x^2
I=0          # x^3

f1=3*sp.pi;    #6*sp.pi # Here is not frequency but pulsation, it should be written omega_1
f11=6*sp.pi;    #6*sp.pi
f12=6*sp.pi;    #6*sp.pi
f2=2*sp.pi;        #2*sp.pi
f3=0;        #0

dA=0;
dH=0;
dI=0;

X_start=0.4;
Y_start=-0.3;
Z_start=0.65;

depth=1e-5; # trajectory depth in real space X
width=0.7; # trajectory width in real space Y
height=0.2 # trajectory height in real space Z

# Curvilinear abscissa
s = sp.Symbol('s') 
s_eval=np.arange(0,1,1/nb_points);
s_max = 1
s_min = 0
S_Interval=Interval(s_min,s_max)

# THEORETICAL FUNCTIONS
def X_t(s):
    return 1e-5*s
    #return 1e-5*s+A*s+B*sp.sin(f1*s)+C*sp.cos(f1*s)
    #return A*s+B*sp.sin(f1*s)+C*sp.cos(f1*s)
def Y_t(s):
    return s
def Z_t(s):
    #return 2*s
    return A*(dA+s)+B*sp.sin(f1*s)+C*sp.cos(f1*s)+D*sp.sin(f2*s)**2+E*sp.cos(f2*s)**2+F*sp.sin(f3*s)**3+G*sp.cos(f3*s)**3 + H*(dH+s)**2 + I*(dI+s)**3;
    #return A*s+B*sp.atan(sp.atan(sp.cos(f11*s)))+C*sp.atan(sp.atan(sp.cos(f12*s)))+D*sp.atan(sp.atan(sp.sin(f2*s)))**2+E*sp.atan(sp.atan(sp.cos(f2*s)))**2+F*sp.atan(sp.atan(sp.sin(f3*s)))**3+G*sp.atan(sp.atan(sp.cos(f3*s)))**3;

# COMPUTING
splot.plot_parametric((Y_t(s),Z_t(s)),(s,s_min,s_max),xlabel="y_t(s)",ylabel='z_t(s)')
splot.plot_parametric((Y_t(s),X_t(s)),(s,s_min,s_max),xlabel="y_t(s)",ylabel='x_t(s)')
splot.plot3d_parametric_line(X_t(s),Y_t(s),Z_t(s),(s,s_min,s_max),xlabel="x_t(s)",ylabel='y_t(s)',zlabel='z_t(s)')
print("x_t(s) = ",X_t(s))
print("y_t(s) = ",Y_t(s))
print("z_t(s) = ",Z_t(s))

# Computing Max Min of theoretical functions
X_t_max = sp.maximum(X_t(s), s, S_Interval)
print('X_t_max =\t',X_t_max)
X_t_min = sp.minimum(X_t(s), s, S_Interval)
print('X_t_min =\t',X_t_min)
Y_t_max = sp.maximum(Y_t(s), s, S_Interval)
print('Y_t_max =\t',Y_t_max)
Y_t_min = sp.minimum(Y_t(s), s, S_Interval)
print('Y_t_min =\t',Y_t_min)
Z_t_max = 2 #sp.maximum(Z_t(s), s, S_Interval) # too complicated to compute
print('Z_t_max =\t',Z_t_max)
Z_t_min = 0 #sp.minimum(Z_t(s), s, S_Interval) # too complicated to compute
print('Z_t_min =\t',Z_t_min)

# DERIVATIVE
dx_t=sp.diff(X_t(s),s)
dy_t=sp.diff(Y_t(s),s)
dz_t=sp.diff(Z_t(s),s)

splot.plot(dz_t,(s,0,1),xlabel="s",ylabel='dz_t(s)')
print("\ndx_t(s)/ds = ",dx_t)
print("dy_t(s)/ds = ",dy_t)
print("dz_t(s)/ds = ",dz_t)


# In[14]:


# REAL FUNCTIONS
def X_r(s):
    return (X_t(s)-X_t(0)) * depth / (X_t_max-X_t_min) + X_start
def Y_r(s):
    return (Y_t(s)-Y_t(0)) * width / (Y_t_max-Y_t_min) + Y_start
def Z_r(s):
    return (Z_t(s)-Z_t(0)) * height / (Z_t_max-Z_t_min) + Z_start

# COMPUTING
splot.plot(X_r(s),(s,s_min,s_max),xlabel="s",ylabel='x_r(s)')
splot.plot(Y_r(s),(s,s_min,s_max),xlabel="s",ylabel='y_r(s)')
splot.plot(Z_r(s),(s,s_min,s_max),xlabel="s",ylabel='z_r(s)')
splot.plot(Z_r(s),(s,0.5,0.7),xlabel="s",ylabel='z_r(s)')
splot.plot_parametric((Y_r(s),Z_r(s)),(s,s_min,s_max),xlabel="y_r(s)",ylabel='z_r(s)')
splot.plot3d_parametric_line(X_r(s),Y_r(s),Z_r(s),(s,s_min,s_max),xlabel="x_r(s)",ylabel='y_r(s)',zlabel='z_r(s)')
print("x_r(s) = ",X_r(s))
print("y_r(s) = ",Y_r(s))
print("z_r(s) = ",Z_r(s))

x_r=X_r(s)
y_r=Y_r(s)
z_r=Z_r(s)

# DERIVATIVE
dx_r=sp.diff(X_r(s),s)
dy_r=sp.diff(Y_r(s),s)
dz_r=sp.diff(Z_r(s),s)

splot.plot(dz_r,(s,0,1),xlabel="s",ylabel='dz_r(s)')
print("\ndx_r(s)/ds = ",dx_r)
print("dy_r(s)/ds = ",dy_r)
print("dz_r(s)/ds = ",dz_r)

# DOUBLE DERIVATIVE
ddx_r=sp.diff(dx_r,s)
ddy_r=sp.diff(dy_r,s)
ddz_r=sp.diff(dz_r,s)

splot.plot(ddz_r,(s,0,1),xlabel="s",ylabel='ddz_r(s)')
print("\nddx_r(s)/dds = ",ddx_r)
print("ddy_r(s)/dds = ",ddy_r)
print("ddz_r(s)/dds = ",ddz_r)

# Curvature
ddx_r=sp.diff(dx_r,s)
ddy_r=sp.diff(dy_r,s)
ddz_r=sp.diff(dz_r,s)

splot.plot(ddz_r,(s,0,1),xlabel="s",ylabel='ddz_r(s)')
print("\nddx_r(s)/dds = ",ddx_r)
print("ddy_r(s)/dds = ",ddy_r)
print("ddz_r(s)/dds = ",ddz_r)


# Angle
alpha_x_r=sp.atan(dx_r)
alpha_y_r=sp.atan(dy_r)
alpha_z_r=sp.atan(dz_r)

splot.plot(alpha_z_r,(s,0,1),xlabel="s",ylabel='alpha_z_r(s)')
print("\nalpha_x_r(s) = ",alpha_x_r)
print("alpha_y_r(s) = ",alpha_y_r)
print("alpha_z_r(s) = ",alpha_z_r)


# Angle Derivative
dalpha_x_r=sp.diff(alpha_x_r,s)
dalpha_y_r=sp.diff(alpha_y_r,s)
dalpha_z_r=sp.diff(alpha_z_r,s)

splot.plot(dalpha_z_r,(s,0,1),xlabel="s",ylabel='dalpha_z_r(s)')
print("\ndalpha_x_r(s) = ",dalpha_x_r)
print("dalpha_y_r(s) = ",dalpha_y_r)
print("dalpha_z_r(s) = ",dalpha_z_r)


# In[10]:


Z_r(s)
#sp.simplify(Z_r(s))


# ### Tangent to tangent max angle computation

# In[31]:


# DERIVATIVE SIMPLIFIED
dx_r_simple=sp.series(dx_r,s,n=4)
dy_r_simple=sp.series(dy_r,s,n=4)
dz_r_simple=sp.series(dz_r,s,n=4)

dx_r_simple=sp.simplify(dx_r_simple)
dy_r_simple=sp.simplify(dy_r_simple)
dz_r_simple=sp.simplify(dz_r_simple)

norme_simple=sp.simplify(sp.sqrt(dx_r_simple**2+dy_r_simple**2+dz_r_simple**2))

t_x=sp.simplify(dx_r_simple/norme_simple)
t_y=sp.simplify(dy_r_simple/norme_simple)
t_z=sp.simplify(dz_r_simple/norme_simple)

splot.plot(dz_r,(s,0,1),xlabel="s",ylabel='dz_r(s)')
print("\ndx_r(s)/ds = ",dx_r)
print("dy_r(s)/ds = ",dy_r)
print("dz_r(s)/ds = ",dz_r)
print("\ndx_r(s)/ds ~ ",dx_r_simple)
print("dy_r(s)/ds ~ ",dy_r_simple)
print("dz_r(s)/ds ~ ",dz_r_simple)

print("norme_simple ~ ",norme_simple)


print("\ntx ~ ",t_x)
print("ty ~ ",t_y)
print("tz ~ ",t_z)

### Newton-Raphson tangent to tangent angle computation
# In[5]:


s_tab=[]
s_tab.append(0)

e_tab=[]
ds_dist_tab=[]
ds_angle_tab=[]
ds_min=1

while(s_tab[-1] < 1):
    s_init = s_tab[-1]
    t0_x = (dx_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)).evalf(subs={s:s_init})
    t0_y = (dy_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)).evalf(subs={s:s_init})
    t0_z = (dz_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)).evalf(subs={s:s_init})

    t1_x = dx_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)
    t1_y = dy_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)
    t1_z = dz_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)

    d_max = 1.7e-3
    alpha_max = 2.5e-3
    ds_dist = (d_max/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)).evalf(subs={s:0})
    #################################################################""
    e=1
    s_prev=s_init+ds_dist

    f_opt = (t0_x*t1_x + t0_y*t1_y + t0_z*t1_z) - cos(alpha_max)

    df_opt = sp.diff(f_opt,s)

    while (e>0.01e-3):
        s_next = s_prev - f_opt.evalf(subs={s:s_prev})/df_opt.evalf(subs={s:s_prev})
        e = abs(s_next-s_prev)
        s_prev = s_next
        #print(e)
        e_tab.append(e)

    ds_angle = s_next-s_init
    
    
    #ds=min(ds_angle,ds_dist)
    #print(s_init+ds)
    
    if (0<ds_angle<ds_dist):
        ds=ds_angle
        print(s_init+ds,"\tAngle")
    elif(0<ds_dist<ds_angle):
        ds=ds_dist
        print(s_init+ds,"\tDist") 
    else:
        print("ERREUR")
    
    if (ds<ds_min):
        ds_min=ds
        
    if (ds<0):
        print("\ts_init=",s_init) #s_init= 0.259787985782348 ERREUR
        print("\ts_next_angle=",s_next)
        print("\ts_next_dist=",s_init+ds_dist)
        
        
    s_tab.append(s_init+ds)
    #print("\nds_dist = ",ds_dist)
    ds_dist_tab.append(ds_dist)
    #print("ds_angle = ",ds_angle)
    ds_angle_tab.append(ds_angle)
print("The Minimum sample length is ds=",ds_min)
print("s list size=",ds_min)

s_init = 0.570311303926697 #0.259787985782348
t0_x = (dx_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)).evalf(subs={s:s_init})
t0_y = (dy_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)).evalf(subs={s:s_init})
t0_z = (dz_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)).evalf(subs={s:s_init})

t1_x = dx_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)
t1_y = dy_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)
t1_z = dz_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)

d_max = 1.7e-3
alpha_max = 2.5e-3
ds_dist = (d_max/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)).evalf(subs={s:0})
#################################################################""
e=1
s_prev=s_init+ds_dist

f_opt = (t0_x*t1_x + t0_y*t1_y + t0_z*t1_z) - cos(alpha_max)

df_opt = sp.diff(f_opt,s)
splot.plot(f_opt,(s,0.56,0.58),xlabel="s",ylabel='f_opt')

#0.264037985782348

#s_init= 0.570311303926697
#s_next_angle= 0.568018191296438
#s_next_dist= 0.574561303925369plt.plot(s_tab)
plt.xlabel("$P_i$ points")
plt.ylabel("Curvilinear abscissa s ")
plt.suptitle("Evolution of curvilinear abscissa depending on sample value along path\nNewton Raphson with Robot limits\nk = "+str(k)+"\n"+r"$\beta$ = "+str(beta)+"\nf = "+str(f)+" Hz\nTrajectory n°"+str(n_path))
#### Newton Raphson algorithm test
x = sp.Symbol('x') 

def f_test(x):
     return x**5 - 2*x**4 + 1.5*x**3 - 4*x +2.5
        
splot.plot(f_test(x),(x,-2,2),xlabel="x",ylabel='f_test') 

x_start=-0.8

e=1
nb_ite=0
while(e>1e-9):
    nb_ite+=1
    x_next=x_start-f_test(x).evalf(subs={x:x_start})/sp.diff(f_test(x),x).evalf(subs={x:x_start})
    e=abs(x_next-x_start)
    print("x_start = ",x_start,"\t// e = ",e,"\t// ite ",nb_ite)
    x_start=x_next
# ## Path Sampling considering 2/3 Power Law and robot limits - OFFLINE

# In[ ]:


s_tab_PL=[]
# 2/3 Power Law parameters
k = 10
beta = 1/3
f = 1000

ds_min=1

# Robot limits
d_max = 1.7e-3
alpha_max = 2.5e-3

xd_norm = sp.sqrt(dx_r**2 + dy_r**2 + dz_r**2)
xdd_norm = sp.sqrt(ddx_r**2 + ddy_r**2 + ddz_r**2)

def c(s):
    return ((xd_norm**2 * xdd_norm**2 - (dx_r*ddx_r + dy_r*ddy_r + dz_r * ddz_r)**2)/xd_norm**6)**(1/2)

def r(s):
    
    return 1 / c(s)

def v(s):
    return k * r(s) ** beta

def alpha(s0,s1):
    t0_x = (dx_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)).evalf(subs={s:s0})
    t0_y = (dy_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)).evalf(subs={s:s0})
    t0_z = (dz_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)).evalf(subs={s:s0})

    t1_x = (dx_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)).evalf(subs={s:s1})
    t1_y = (dy_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)).evalf(subs={s:s1})
    t1_z = (dz_r/sp.sqrt(dx_r**2+dy_r**2+dz_r**2)).evalf(subs={s:s1})
    
    return sp.acos(t0_x*t1_x + t0_y*t1_y + t0_z*t1_z)

def d(s0,s1):
    dx = X_r(s).evalf(subs={s:s1})-X_r(s).evalf(subs={s:s0})
    dy = Y_r(s).evalf(subs={s:s1})-Y_r(s).evalf(subs={s:s0})
    dz = Z_r(s).evalf(subs={s:s1})-Z_r(s).evalf(subs={s:s0})
    
    return sp.sqrt(dx**2 + dy**2 + dz**2)

splot.plot(c(s),(s,0,1),xlabel="s",ylabel='c(s)')



s_ini = 0
s_tab_PL.append(s_ini)
while (s_ini<1):
    if (c(s).evalf(subs={s:s_ini})>0):
        ds = min((v(s) / f).evalf(subs={s:s_ini}),d_max)
    else:
        ds = d_max
    
    if (alpha(s_ini,s_ini+ds) > alpha_max):
        print("\n--------------------Don't Fit Robot Angle Limits-------------------------")
        print("ds = ",ds,"\t||\td_max = ",d_max)
        print("alpha = ",alpha(s_ini,s_ini+ds),"\t||\talpha_max = ",alpha_max)
        s_ini=0
        k = k/2
        ds_min=1
        print("new k = ",k)
        
        print("---------------------------------------------------------------------------")
    elif (d(s_ini,s_ini+ds) > d_max):
        print("\n--------------------Don't Fit Robot Speed Limits-------------------------")
        print("ds = ",ds)
        print("alpha = ",d(s_ini,s_ini+ds),"\t||\talpha_max = ",d_max)
        s_ini=0
        k = k/2
        ds_min=1
        print("new k = ",k)
        
        print("---------------------------------------------------------------------------")
    else:
        s_ini=s_ini+ds
        if(ds<ds_min):
            ds_min=ds
    print("s = ",s_ini,"   \t||\tds = ",ds,"\t||\td = ",d(s_ini,s_ini+ds),"\t||\t k = ",k)
    s_tab_PL.append(s_ini)
print("ds_min = ",ds_min)
# ## Path Sampling considering 2/3 Power Law and robot limits - ONLINE

# In[ ]:


# Find the curvilinear abscissa corresponding to the closest point from the current point on the path
nb_seg = 1000
delta_s=0.001

p_curr = np.array([0 , 0 , 0.64])
dp_curr = np.array([0,0.03,0])
d_i_tab=[]
s_i_tab=[]

#identification of the closest point
for i in range(int(1/delta_s)):
    s_i=i * delta_s
    p_i=np.array([np.float64(X_r(s).evalf(subs={s:s_i})),np.float64(Y_r(s).evalf(subs={s:s_i})),np.float64(Z_r(s).evalf(subs={s:s_i}))])
    dp=p_i-p_curr
    d_i=np.linalg.norm(dp)
    s_i_tab.append(s_i)
    d_i_tab.append(d_i)
    

s_i=s_i_tab[d_i_tab.index(min(d_i_tab))]
print('The closest point is at abscissa s_i=',s_i)

c_i=c(s).evalf(subs={s:s_i})
print('Curvature at this point is c_i=',c_i)

v_i=np.linalg.norm(dp_curr)
print('Current speed is v_i=',v_i,'m/s')

k_i=v_i*c_i**(1/3)
print('2/3 Power Law parameter k_i=',k_i,'SI') 


# ### Solve s*=argmin(distp + distR)
# 

# In[6]:


X=[X_r(s),Y_r(s),Z_r(s)]
Xdes_p=np.array([    0.499029,   -0.101256,    0.582249])
Xdes_M=np.array([ [ 1.26572e-07,-6.35024e-07, 1.80791e-06],
 [-2.06137e-06,-5.59844e-06,-8.23298e-06],
  [7.19602e-07, 8.36069e-06,-5.65037e-06]])

dist_p = sp.sqrt((X[0]-Xdes_p[0])**2+(X[1]-Xdes_p[1])**2+(X[2]-Xdes_p[2])**2)
d_dist_p=sp.diff(dist_p,s)
splot.plot(dist_p,(s,0,1),xlabel="s",ylabel='dist_p(s)')
splot.plot(d_dist_p,(s,0,1),xlabel="s",ylabel='ddist_p(s)')


# In[27]:


X=[X_r(s),Y_r(s),Z_r(s)]
Xdes_p=[    0.499528,   -0.199835,    0.500334]
Xdes_M=[ [ 1.26572e-07,-6.35024e-07, 1.80791e-06],
 [-2.06137e-06,-5.59844e-06,-8.23298e-06],
  [7.19602e-07, 8.36069e-06,-5.65037e-06]]

def dist_p(s):
    return sp.sqrt((X[0]-Xdes_p[0])**2+(X[1]-Xdes_p[1])**2+(X[2]-Xdes_p[2])**2)
d_dist_p=sp.diff(dist_p(s),s)
splot.plot(dist_p(s),(s,-1,1),xlabel="s",ylabel='dist_p(s)')
splot.plot(d_dist_p,(s,0,1),xlabel="s",ylabel='ddist_p(s)')
print(dist_p(0.5))


# In[ ]:



r12=dx_r
r22=dy_r
r32=dz_r

r13=0
r23=1/sp.sqrt((r22/r32)**2+1)
r33=-r23*r22/r32



r11 = r22*r33 - r32*r23
r21 = r32*r13 - r12*r33
r31 = r12*r23 - r22*r13

X_path=np.array([[r11, r12, r13],
                 [r21, r22, r23],
                 [r31, r32, r33]])

X_prod = np.dot(inv(Xdes_M),X_path)
print("\n",inv(Xdes_M))
print("\n",Xdes_M)
print("\n",r11.evalf(subs={s:0.2}))
print("\n",X_prod)
rA = X_prod[0][0]
print("\n",rA)
rB = X_prod[1][1]
print("\n",rB)
rC = X_prod[2][2]
print("\n",rC)
#rA = r11 * Xdes_M[0][0] + r21 * Xdes_M[0][1] + r31 * Xdes_M[0][2]
#rB = r12 * Xdes_M[1][0] + r22 * Xdes_M[1][1] + r32 * Xdes_M[1][2]
#rC = r13 * Xdes_M[2][0] + r23 * Xdes_M[2][1] + r33 * Xdes_M[2][2]

dist_R = sp.acos((rA+rB+rC-1)/2)
d_dist_R=sp.diff(dist_R,s)
splot.plot(dist_R,(s,0,1),xlabel="s",ylabel='dist_R(s)')
splot.plot(d_dist_R,(s,0,1),xlabel="s",ylabel='ddist_R(s)')


# In[ ]:


def dist_R(s):
    
    r12=dx_r.evalf(subs={s:s})
    r22=dy_r.evalf(subs={s:s})
    r32=dz_r.evalf(subs={s:s})
    
    # NORMAL VECTOR
    r13 = 0 # on the plane Oy_0z_0, orthogonal to x_0 vector
    if(r32==0):
        r23=0
        r33=1
    else:
        r23 = sp.sqrt(1/((r22/r32)**2+1))
        r33 = -r23 * r22 / r32
    if (r33>0):
        r13=-r13
        r23=-r23
        r33=-r33


    r11 = r22*r33 - r32*r23
    r21 = r32*r13 - r12*r33
    r31 = r12*r23 - r22*r13

    rA = r11 * Xdes_M[0][0] + r21 * Xdes_M[0][1] + r31 * Xdes_M[0][2]
    rB = r12 * Xdes_M[1][0] + r22 * Xdes_M[1][1] + r32 * Xdes_M[1][2]
    rC = r13 * Xdes_M[2][0] + r23 * Xdes_M[2][1] + r33 * Xdes_M[2][2]

    return sp.acos((rA+rB+rC-1)/2)
    
d_dist_R=sp.diff(dist_R,s)
splot.plot(dist_R,(s,0,1),xlabel="s",ylabel='dist_R(s)')
splot.plot(d_dist_R,(s,0,1),xlabel="s",ylabel='ddist_R(s)')


# In[ ]:


rho=0.1
dist_frame=rho*dist_p+(1-rho)*dist_R
dist_frame
d_dist_frame=sp.diff(dist_frame,s)
splot.plot(dist_frame,(s,0,1),xlabel="s",ylabel='dist(s)')
splot.plot(d_dist_frame,(s,0.155,0.17),xlabel="s",ylabel='ddist(s)')


# ### Find a minimum value in dist_frame

# In[ ]:


# Sampling interval
delta_s=0.005
s_star=0
d_star=dist_frame.evalf(subs={s:s_star})

for i in range(int(1/delta_s)):
    #print(i)
    d=dist_frame.evalf(subs={s:(i * delta_s)})
    #print("d= ",d,"\ts=",i * delta_s)
    if (d==sp.nan):
        True
        #print('NaN')
    elif (d<d_star):
        d_star = d
        s_star = i * delta_s
        #print("d*= ",d_star,"\ts*= ",s_star)
print("s* approx = ",s_star)


# #### Méthode de Newton-Raphson

# In[25]:


splot.plot3d_parametric_line(X_r(s),Y_r(s),Z_r(s),(s,-2,2),xlabel="x_r(s)",ylabel='y_r(s)',zlabel='z_r(s)')


# In[17]:


d_dist_p


# In[21]:


Xdes_p


# In[23]:


s_star=0


# In[24]:


# Find precise solution with Newton Raphson by solving d_dist_frame=0
e_tab_star=[]
e=1
s_prev = s_star
#s_prev=s_init+ds_dist

f_opt = d_dist_p

df_opt = sp.diff(f_opt,s)

while (e>0.01e-3):
    s_star = s_prev - f_opt.evalf(subs={s:s_prev})/df_opt.evalf(subs={s:s_prev})
    X=[X_r(s_star),Y_r(s_star),Z_r(s_star)]
    print(X)
    e = abs(s_star-s_prev)
    s_prev = s_star
    print("s = ",s_star,"\te = ",e)
    e_tab_star.append(e)
print("s* = ",s_star)


# #### Méthode de la sécante

# In[ ]:


s_star=0.163


# In[ ]:


# Find precise solution with Secant Method by solving d_dist_frame=0
e_tab_star=[]
e=1
s_prev_0 = s_star+delta_s #n
s_prev_1 = s_star #n-1
#s_prev=s_init+ds_dist

f_opt = d_dist_frame

df_opt = sp.diff(f_opt,s)

while (e>0.01e-3):
    s_star = s_prev_0 - f_opt.evalf(subs={s:s_prev_0})*(s_prev_0-s_prev_1)/(f_opt.evalf(subs={s:s_prev_0})-f_opt.evalf(subs={s:s_prev_1})) #n+1
    e = abs(s_star-s_prev_0)
    s_prev_1=s_prev_0
    s_prev_0 = s_star
    print("s = ",s_star,"\te = ",e)
    e_tab_star.append(e)
print("s* = ",s_star)


# In[ ]:


e_tab_star


# In[ ]:


lower_bound = 0
upper_bound = 3.5
#function = (x**3/3) - (2*x**2) - 3*x + 1
function=d_dist_p


zeros = solveset(function, s, domain=Interval(lower_bound, upper_bound))
assert zeros.is_FiniteSet # If there are infinite solutions the next line will hang.
ans = Min(function.subs(s, lower_bound), function.subs(s, upper_bound), *[function.subs(s, i) for i in zeros])
ans


# # Compute frame on each point of the curve

# In[ ]:


abscissa_test=0

# TANGENT VECTOR
r12 = dx_r.evalf(subs={s:abscissa_test})
r22 = dy_r.evalf(subs={s:abscissa_test})
r32 = dz_r.evalf(subs={s:abscissa_test})

normT=sp.sqrt(r12**2+r22**2+r32**2)
r12 = r12/normT
r22 = r22/normT
r32 = r32/normT

print("T = \n\t",r12,"\n\t",r22,"\n\t",r32)

# NORMAL VECTOR
r13 = 0 # on the plane Oy_0z_0, orthogonal to x_0 vector
if(r32==0):
    r23=0
    r33=1
else:
    r23 = sp.sqrt(1/((r22/r32)**2+1))
    r33 = -r23 * r22 / r32
if (r33>0):
    r13=-r13
    r23=-r23
    r33=-r33
    
#r23 = sp.sqrt(1/((r22/r12)**2+1))
#r13 = -r23 * r22 / r12  # on the plane Oy_0x_0, orthogonal to x_0 vector
#r33 = 0 # on the plane Oy_0x_0, orthogonal to x_0 vector
#if (r13<0):
#    r13=-r13
#    r23=-r23
#    r33=-r33

print("\nN = \n\t",r13,"\n\t",r23,"\n\t",r33)


# 2nd NORMAL VECTOR
r11 = r22*r33 - r32*r23
r21 = r32*r13 - r12*r33
r31 = r12*r23 - r22*r13
print("\nN2 = \n\t",r11,"\n\t",r21,"\n\t",r31)

# Vectors norms
print("\nT norm = \t", sp.sqrt(r12**2+r22**2+r32**2))
print("N norm = \t", sp.sqrt(r13**2+r23**2+r33**2))
print("N2 norm = \t", sp.sqrt(r11**2+r21**2+r31**2))

R=[[r11, r12, r13],[r21, r22, r23],[r31, r32, r33]]
print("\nR = \n", R[0],"\n", R[1],"\n", R[2])

detR=R[0][1]*R[1][2]*R[2][0]-R[0][1]*R[2][2]*R[1][0]+R[1][1]*R[2][2]*R[0][0]-R[1][1]*R[0][2]*R[2][0]+R[2][1]*R[0][2]*R[1][0]-R[2][1]*R[1][2]*R[0][0]
print("\ndet(R) = ", detR)


# In[ ]:





# In[ ]:




